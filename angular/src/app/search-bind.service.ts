import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SearchBindService {
  private title: String;
  
  constructor(public http : Http) { 
    console.log("Service works...");
  }



  getLinks(){
    return this.http.get('http://ec2-18-222-113-143.us-east-2.compute.amazonaws.com:9002/rest/api/templates/search?title='+this.title)
    .pipe(map(res => res.json()));
    }

  updateTemplate( title : string )
  {
	         this.title = title;
  }
   
}
