import { TestBed, inject } from '@angular/core/testing';

import { SearchBindService } from './search-bind.service';

describe('SearchBindService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchBindService]
    });
  });

  it('should be created', inject([SearchBindService], (service: SearchBindService) => {
    expect(service).toBeTruthy();
  }));
});
