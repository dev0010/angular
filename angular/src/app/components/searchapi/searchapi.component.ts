import { Component, OnInit, ViewEncapsulation, KeyValueDiffers, DoCheck, ViewChild, ElementRef } from '@angular/core';

import { SearchBindService } from 'app/search-bind.service';

@Component({
  selector: 'app-searchapi',
  templateUrl: './searchapi.component.html',
  styleUrls: ['./searchapi.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchapiComponent implements OnInit, DoCheck {
  @ViewChild('selectedOption') selectedOption: ElementRef;
  @ViewChild('dynamicTitle') dynamicTitle: ElementRef;
  posts : any [] = [];
  getOptions : Array<any> = [];
  pages : Array<any> = [];
  locator : Array<any> = [];
  selected : boolean = false;
  title : any;
  locatorTitle : string = "";
  differ : any;

  constructor(
    private differs: KeyValueDiffers,
    private searchbindService : SearchBindService
  ) {
    
  }
  
  ngDoCheck() {
   
  }

  ngOnInit() {
  }

  getSearch(){

   

    this.searchbindService.updateTemplate(this.title);
    this.searchbindService.getLinks().subscribe(posts => { 
    
    this.getOptions = posts;
    });
  }

  setLocatorEditable() {


console.log('click')

this.searchbindService.getPage().subscribe(pages => {
  console.log(pages.content)
  this.pages = pages.content
});
  }

  editSelected(text) {
    if(text.includes('{{ui-locator}}')) {
      text = text.replace('{{ui-locator}}', `<span id="ui-locator"> `+`{{locatorTitle}}`+`</span>`);
      
      console.log(text);
     
    }
    return '<p>'+text+'</p>';
  }

  onSelect(title) {
    this.selected = true;

    this.getOptions = [];
    this.selected = true;
    this.locatorTitle = "uiLocator";



    const divideStr = title.split('{{ui-locator}}')
    console.log(divideStr, title)
    console.log(this.selectedOption)
    this.selectedOption.nativeElement.insertAdjacentHTML('beforebegin', divideStr[0])
    this.selectedOption.nativeElement.insertAdjacentHTML('afterend', divideStr[1])
    
    this.getOptions = [];

    

  }

  onSelectPage(page){
    console.log(page)
    this.locatorTitle = page.pageName
    this.pages = []
    this.searchbindService.getLocator(page.pageId).subscribe(locator => {
      console.log(locator)
      this.locator = locator.content
    });
  }

  onSelectLocator(locator){
    console.log(locator)
    this.locatorTitle = locator.locatorName
    this.locator = []
    
    console.log(this.dynamicTitle.nativeElement.innerText, "hahahah")
  }

  onClick(){
    if(!this.selected){
    this.posts.push(this.title);
    this.title = '';
  } else {
    this.posts.push(this.dynamicTitle.nativeElement.innerText);
    this.title = '';
    this.locatorTitle = ''
    this.dynamicTitle.nativeElement.value = ''
  }
  this.selected = false;
    
  }
}
